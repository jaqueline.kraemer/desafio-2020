from pad_649.find_short import *
import unittest

class TestFindShort(unittest.TestCase):

    def test_find_short(self):
        self.assertEqual(find_short("bitcoin take over the world maybe who knows perhaps"), 3)
        self.assertIsInstance(find_short("bitcoin take over the world maybe who knows perhaps"), int)
        self.assertEqual(find_short("banana"), 6)

        # Bug retorno do número dos espaços
        self.assertEqual(find_short("      "), 3999996)

        self.assertEqual(find_short(123), "error")
        self.assertEqual(find_short(1.23), "error")
        self.assertEqual(find_short(()), "error")
        self.assertEqual(find_short([]), "error")
        self.assertEqual(find_short({}), "error")