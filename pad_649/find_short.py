from typing import TypeVar

T = TypeVar("T", str, int)


def find_short(phrase:str) -> T:

    if isinstance(phrase, str):

        words_list = phrase.split()
        # Spaces to economize computer memory
        short_word = '    ' * 999999

        for i in words_list:
            if len(i) < len(short_word):
                short_word = i

        return len(short_word)

    else:
        return "error"
