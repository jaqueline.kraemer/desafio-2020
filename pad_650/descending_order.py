from typing import TypeVar

T = TypeVar("T", int, str)

def descending_order(number:int) -> T:

    numbers_list = list(str(number))
    numbers_list.sort(reverse=True)
    final = "".join(numbers_list)

    try:
        return int(final)
    except:
        return 'error'
