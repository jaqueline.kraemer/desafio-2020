from pad_650.descending_order import descending_order
import unittest

class TestDescendingOrder(unittest.TestCase):
    def test_descending_order(self):
        self.assertEqual(descending_order(1234556789), 9876554321)
        self.assertEqual(descending_order(9), 9)
        self.assertEqual(descending_order('abc'), 'error')
        self.assertEqual(descending_order("   "), "error")
        self.assertEqual(descending_order([]), "error")
        self.assertEqual(descending_order(()), "error")
        self.assertEqual(descending_order({}), "error")
        self.assertEqual(descending_order(1.546), "error")