import unittest
from pad_652.double_sort import double_sort

class TestDoubleSort(unittest.TestCase):

    def test_double_sort(self):
        self.assertEqual(double_sort(["Banana", "Orange", "Apple", "Mango", 0, 2, 2]), [0, 2, 2, "Apple", "Banana", "Mango", "Orange"])
        self.assertEqual(double_sort(["154584", 0, 2, 2]), [0, 2, 2, "154584"])
        self.assertEqual(double_sort([0, 2, 2, 1.3]), [0, 1.3, 2, 2])
        self.assertEqual(double_sort(["Banana", "@#", "Apple", "Mango", 0, 2, 2]), [0, 2, 2, "@#", "Apple", "Banana", "Mango"])
        self.assertEqual(double_sort(["Banana", "@#", "3221", "Apple", "Mango", 0, 2, 2]), [0, 2, 2, "3221", "@#", "Apple", "Banana", "Mango"])
        self.assertEqual(double_sort(3545), "error")
        self.assertEqual(double_sort("Banana"), "error")
        self.assertEqual(double_sort(("Banana", "Apple")), "error")
        self.assertEqual(double_sort({"Fruta":"Banana", "Preço":60.0}), "error")