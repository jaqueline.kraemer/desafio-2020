from typing import TypeVar

T = TypeVar("T", str, list)


def double_sort(matrix:list) -> list:

    if isinstance(matrix, list):

        numbers = []
        strings = []

        for i in matrix:

            if isinstance(i, float) or isinstance(i, int):
                numbers.append(i)
            elif isinstance(i, str):
                strings.append(i)

        numbers.sort()
        strings.sort()
        final = numbers + strings

        return final

    else:
        return "error"