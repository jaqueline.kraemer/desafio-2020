from typing import TypeVar

T = TypeVar("T", str, int)


def iq_test(numbers: str) -> T:

    try:
        numbers_list = numbers.split()
        pair = []
        odd = []

        for i in numbers_list:
            if int(i) % 2 == 0:
                pair.append(i)
            elif int(i) % 2 == 1:
                odd.append(i)

    except:
        return "error"

    if len(odd) == 1 and len(pair) > 1:
        different = odd[0]
    elif len(pair) == 1 and len(odd) > 1:
        different = pair[0]
    else:
        return "error"

    return numbers_list.index(different) + 1

