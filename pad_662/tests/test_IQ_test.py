import unittest
from pad_662.IQ_test import iq_test

class TestIQ(unittest.TestCase):
    def test_iq_test(self):
        self.assertEqual(iq_test("2 4 7 8 10"), 3)
        self.assertEqual(iq_test("1 3 6 5 9"), 3)
        self.assertEqual(iq_test("2 3 7 8 10"), "error")
        self.assertEqual(iq_test("23211"), "error")
        self.assertEqual(iq_test("a b e c d"), "error")
        self.assertEqual(iq_test(()), "error")
        self.assertEqual(iq_test({}), "error")
        self.assertEqual(iq_test(15464), "error")
        self.assertEqual(iq_test("@#"), "error")
