from pad_651.square_every import *
import unittest

class TestSquareEvery(unittest.TestCase):

    def test_square_every(self):
        self.assertEqual(square_every(6), 36)
        self.assertEqual(square_every(6655), 36362525)
        self.assertEqual(square_every(0), 0)
        self.assertEqual(square_every(1.65876), "error")
        self.assertEqual(square_every([]), "error")
        self.assertEqual(square_every(()), "error")
        self.assertEqual(square_every({}), "error")
        self.assertEqual(square_every("Teste string"), "error")