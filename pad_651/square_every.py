from typing import TypeVar

T = TypeVar("T", str, int)

def square_every(number:int) -> T:

    final = ""

    if isinstance(number, int):

        for i in str(number):
            final += str(int(i)**2)

        return int(final)
    else:
        return "error"
