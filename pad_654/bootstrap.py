from pad_654.app.ships_in_movement import ShipsInMovement

class Startup:
    def execute(self):
        shipsinmovement = ShipsInMovement()
        shipsinmovement.set_angles(90, 30)
        if not shipsinmovement.exceptions():
            shipsinmovement.set_angle_calculation()
            shipsinmovement.set_conversion()
            shipsinmovement.set_distance_calculation()
            shipsinmovement.set_time_calculation()
            print(shipsinmovement.get_time_calculation())
        else:
            print(shipsinmovement.exceptions())
