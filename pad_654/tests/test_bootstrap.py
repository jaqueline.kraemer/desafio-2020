from pad_654.bootstrap import Startup
from pad_654.app.ships_in_movement import ShipsInMovement
from unittest.mock import MagicMock, patch
import unittest


class TestStartup(unittest.TestCase):

    @patch.object(ShipsInMovement, 'get_time_calculation')
    @patch.object(ShipsInMovement, 'set_time_calculation')
    @patch.object(ShipsInMovement, 'set_distance_calculation')
    @patch.object(ShipsInMovement, 'set_conversion')
    @patch.object(ShipsInMovement, 'set_angle_calculation')
    @patch.object(ShipsInMovement, 'exceptions')
    @patch.object(ShipsInMovement, 'set_angles')
    @patch('pad_654.bootstrap.print')
    def test_execute_with_exception(
            self,
            mock_print,
            mock_angles,
            mock_exceptions,
            mock_set_angle_calculation,
            mock_set_conversion,
            mock_set_distance_calculation,
            mock_set_time_calculation,
            mock_get_time_calculation,

    ):

        mock_angles.set_angles = MagicMock(return_value=None)
        mock_exceptions.exceptions = MagicMock(return_value=True)
        mock_set_angle_calculation.set_angle_calculation = MagicMock(return_value=None)
        mock_set_conversion.set_conversion = MagicMock(return_value=None)
        mock_set_distance_calculation.set_distance_calculation = MagicMock(return_value=None)
        mock_set_time_calculation.set_time_calculation = MagicMock(return_value=None)
        mock_get_time_calculation.get_time_calculation = MagicMock(return_value=None)

        start = Startup()
        start.execute()

        shipsinmovement = ShipsInMovement()        
        self.assertIsInstance(shipsinmovement, ShipsInMovement)
        mock_angles.assert_called_once_with(90, 30)
        mock_exceptions.assert_called_once_with()
        mock_set_angle_calculation.assert_called_once_with()
        mock_set_conversion.assert_called_once_with()
        mock_set_distance_calculation.assert_called_once_with()
        mock_set_time_calculation.assert_called_once_with()
        mock_get_time_calculation.assert_called_once_with()

        mock_print.assert_called_once_with(float('inf'))

    @patch('pad_654.bootstrap.print')    
    def test_execute_without_exception(self, mock_print):
        mock_angles = MagicMock()
        mock_exceptions = MagicMock()
        mock_angle_calculation = MagicMock()
        mock_conversion = MagicMock()
        mock_distance_calculation = MagicMock()
        mock_time_calculation = MagicMock()
        mock_set_time = MagicMock()
        mock_get_time = MagicMock()
        mock_angles.set_angles = MagicMock(return_value=None)
        mock_exceptions.exceptions = MagicMock(return_value=False)
        mock_angle_calculation.set_angles = MagicMock(return_value=None)
        mock_conversion.set_angles = MagicMock(return_value=None)
        mock_distance_calculation.set_angles = MagicMock(return_value=None)
        mock_time_calculation.set_angles = MagicMock(return_value=None)
        mock_set_time.set_angles = MagicMock(return_value=None)
        mock_get_time.set_angles = MagicMock(return_value=None)

        mock_print.assert_called_once_with(18.21)

        shipsinmovement = ShipsInMovement()
        start = Startup()
        start.execute()
        self.assertIsInstance(shipsinmovement, ShipsInMovement)
