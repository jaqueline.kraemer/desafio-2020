from pad_654.app.ships_in_movement import ShipsInMovement
import unittest, pytest

class TestShipsInMovement(unittest.TestCase):

    ship = ShipsInMovement()

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_set_angles(self):

        self.ship.set_angles(90, 0)

        with pytest.raises(Exception) as erro:
            self.ship.set_angles(-90, 180)


        self.assertEqual(str(erro.value), "The number must be from 0° until 359°")

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_exception(self):
        # Entra no if de ângulo oposto
        self.ship.set_angles(200, 20)
        self.assertEqual(self.ship.exceptions(), 12.87472)

        # Entra no if de ângulo igual
        self.ship.set_angles(200, 200)
        self.assertEqual(self.ship.exceptions(), float("inf"))

        self.ship.set_angles(150, 200)
        self.assertFalse(self.ship.exceptions())

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_set_angle_calculation(self):
        self.ship.set_angles(250, 50)
        self.ship.set_angle_calculation()

        self.ship.set_angles(120, 50)
        self.ship.set_angle_calculation()

        self.ship.set_angles(80, 160)
        self.ship.set_angle_calculation()


    def test_get_angles(self):
        self.ship.set_angles(80, 160)
        self.ship.set_angle_calculation()
        self.assertIsInstance(self.ship.get_angle_calculation(), list)
        self.assertEqual(self.ship.get_angle_calculation(), [80, 50.0, 50.0])

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_set_conversion(self):
        self.ship.set_angles(8, 160)
        self.ship.set_angle_calculation()
        self.ship.set_conversion()


    def test_get_conversion(self):
        self.ship.set_angles(8, 160)
        self.ship.set_angle_calculation()
        self.ship.set_conversion()
        self.assertIsInstance(self.ship.get_conversion(), list)
        self.assertEqual(self.ship.get_conversion(), [0.4694715627858907, 0.24192189559966773, 0.24192189559966773])

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_set_distance_calculation(self):
        self.ship.set_angles(8, 160)
        self.ship.set_angle_calculation()
        self.ship.set_conversion()
        self.ship.set_distance_calculation()


    def test_get_distance_calculation(self):
        self.ship.set_angles(8, 160)
        self.ship.set_angle_calculation()
        self.ship.set_conversion()
        self.ship.set_distance_calculation()
        self.assertEqual(self.ship.get_distance_calculation(), 33.172154765159306)

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_opposite_angle_calculation(self):
        self.ship.set_angles(90, 270)
        self.assertEqual(self.ship.opposite_angle_calculation(), 12.87472)

    # ////////////////////////////////////////////////////////////////////////////////////

    def test_set_time_calculation(self):
        self.ship.set_angles(8, 160)
        self.ship.set_angle_calculation()
        self.ship.set_conversion()
        self.ship.set_distance_calculation()
        self.ship.set_time_calculation()


    def test_get_time_calculation(self):
        self.ship.set_angles(8, 160)
        self.ship.set_angle_calculation()
        self.ship.set_conversion()
        self.ship.set_distance_calculation()
        self.ship.set_time_calculation()
        self.assertEqual(self.ship.get_time_calculation(), 13.27)