import math

class ShipsInMovement:

    def set_angles(self, degree_1, degree_2):
        if (degree_1 or degree_2) > 359 or (degree_1 or degree_2) < 0 or type(degree_1) != int or type(degree_2) != int:
            raise Exception("The number must be from 0° until 359°")
        else:
            self.degree_1 = degree_1
            self.degree_2 = degree_2

    # ///////////////////////////////////////////////////////

    def exceptions(self):
        if self.degree_1 > 180:
            opposite = self.degree_1 - 180
        else:
            opposite = 180 - self.degree_1

        if self.degree_1 == self.degree_2:
            return float('inf')
        elif self.degree_2 == opposite:
            return self.opposite_angle_calculation()
        else:
            return False

    # ///////////////////////////////////////////////////////

    def set_angle_calculation(self):
        if self.degree_1 > (180 + self.degree_2):
            angle = 360 - self.degree_1 + self.degree_2
        elif self.degree_1 > self.degree_2:
            angle = self.degree_1 - self.degree_2
        else:
            angle = self.degree_2 - self.degree_1

        same_angle = (180 - angle) / 2
        angles = [angle, same_angle, same_angle]

        self.angles = angles


    def get_angle_calculation(self):
        return self.angles

    # ///////////////////////////////////////////////////////

    def set_conversion(self):
        converted_angle = []
        converted_angle.append(math.sin(math.radians(self.angles[0])))
        converted_angle.append(math.sin(math.radians(self.angles[1])))
        converted_angle.append(math.sin(math.radians(self.angles[2])))

        self.converted_angle = converted_angle


    def get_conversion(self):
        return self.converted_angle

    # ///////////////////////////////////////////////////////

    def set_distance_calculation(self):
        x = self.converted_angle[1] * ((40 * 1.60934) / self.converted_angle[0])

        self.distance_calculated = x


    def get_distance_calculation(self):
        return self.distance_calculated

    # ///////////////////////////////////////////////////////


    def opposite_angle_calculation(self):
        opposite_angles_time = ((20 * 1.60934) / 150) * 60

        return opposite_angles_time

    # ///////////////////////////////////////////////////////

    def set_time_calculation(self):
        final = (self.distance_calculated / 150) * 60

        self.final_time = round(final, 2)


    def get_time_calculation(self):
        return self.final_time

    # ///////////////////////////////////////////////////////
