from typing import TypeVar

T = TypeVar("T", str, int)

def vowel_count(phrase:str) -> T:
    if isinstance(phrase, str):
        vowel = 'aeiouáàãâäéèêëíìîïóòõôöúùûü'
        total = 0
        for i in vowel:
            total += phrase.lower().count(i)

        return total
    else:
        return "error"
