from pad_648.vowel_count import vowel_count
import unittest

class TestVowerCount(unittest.TestCase):
    def test_vowel_count(self):
        self.assertEqual(vowel_count("banana abcefu"), 6)
        self.assertEqual(vowel_count('   '), 0)
        self.assertEqual(vowel_count('bcdfgh'), 0)
        self.assertEqual(vowel_count(123), "error")
        self.assertEqual(vowel_count(1.23), "error")
        self.assertEqual(vowel_count(()), "error")
        self.assertEqual(vowel_count([]), "error")
        self.assertEqual(vowel_count({}), "error")
        self.assertEqual(vowel_count("  "), 0)