from pad_653.growing_plant import *
import unittest

class TestGrowingPlant(unittest.TestCase):

    def test_growing_plant(self):
        self.assertEqual(growing_plant(10, 9, 4), 1)
        self.assertEqual(growing_plant(100, 10, 910), 10)
        self.assertEqual(growing_plant(4, 3, 100), "error")
        self.assertEqual(growing_plant("5", 3, 100), "error")
        self.assertEqual(growing_plant((), 3, 100), "error")
        self.assertEqual(growing_plant(2.5, 3, 100), "error")

    def test_rules(self):
        self.assertEqual(rules(4, 3, 100), False)
        self.assertEqual(rules(10, 1, 100), False)
        self.assertEqual(rules(10, 5, 20000), False)
        self.assertEqual(rules(6, 3, 120), True)

