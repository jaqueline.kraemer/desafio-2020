from typing import TypeVar

T = TypeVar("T", str, int)


def rules(upSpeed, downSpeed, desiredHeight) -> bool:

    if upSpeed < 5 or upSpeed > 100:
        return False
    elif downSpeed < 2 or downSpeed > upSpeed:
        return False
    elif desiredHeight < 4 or desiredHeight > 1000:
        return False
    else:
        return True


def growing_plant(upSpeed:int, downSpeed:int, desiredHeight:int) -> T:
    
    if type(upSpeed) == int and type(downSpeed) == int and type(desiredHeight) == int:
        
        if rules(upSpeed, downSpeed, desiredHeight):

            days = []
            size = 0

            while True:
                size += upSpeed
                days.append(size)
                if size < desiredHeight:
                    size -= downSpeed
                    days.append(size)
                else:
                    break

            day = round((len(days)/2) + 0.5)
            return day

        return "error"

    else:
        return "error"

