def get_middle_letters(word: str) -> str:

    if isinstance(word, str):
        index = (len(word) // 2)
        if len(word) % 2 == 0:
            return f"{word[index - 1]}{word[index]}"
        return f"{word[index]}"
    else:
        return "error"
