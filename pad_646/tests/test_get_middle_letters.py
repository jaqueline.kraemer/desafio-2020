from pad_646.middle_letters import get_middle_letters
import unittest

class TestMiddleLetters(unittest.TestCase):
    def test_get_middle_letters(self):
        self.assertEqual(get_middle_letters("self"), "el")
        self.assertEqual(get_middle_letters("selfing"), "f")
        self.assertEqual(get_middle_letters("middle"), "dd")
        self.assertEqual(get_middle_letters("A"), "A")
        self.assertEqual(get_middle_letters("of"), "of")
        self.assertEqual(get_middle_letters(123), "error")
        self.assertEqual(get_middle_letters(1.23), "error")
        self.assertEqual(get_middle_letters(()), "error")
        self.assertEqual(get_middle_letters([]), "error")
        self.assertEqual(get_middle_letters({}), "error")
        self.assertEqual(get_middle_letters(" "), " ")