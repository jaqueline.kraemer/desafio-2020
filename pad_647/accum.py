def accum(string: str) -> str:

    if isinstance(string, str):
        final = ""
        counter = 1
        
        for i in string:
            new_str = i*counter
            final += f"{new_str.title()}-"
            counter += 1

        final = final[:-1]
        return final
    else:
        return "error"
