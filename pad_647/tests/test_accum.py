from pad_647.accum import accum
import unittest

class TestAccum(unittest.TestCase):

    def test_accum(self):
        self.assertEqual(accum("nnYt"), "N-Nn-Yyy-Tttt")
        self.assertEqual(accum("ABCD"), "A-Bb-Ccc-Dddd")
        self.assertEqual(accum("abcd"), "A-Bb-Ccc-Dddd")
        self.assertEqual(accum(123), "error")
        self.assertEqual(accum(1.23), "error")
        self.assertEqual(accum(()), "error")
        self.assertEqual(accum([]), "error")
        self.assertEqual(accum({}), "error")
        self.assertEqual(accum("  "), " -  ")

