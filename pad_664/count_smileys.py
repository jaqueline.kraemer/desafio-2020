import re


def count_smileys(array:list) -> int:
    counter = 0

    if isinstance(array, list):
        for i in array:
            if isinstance(i, str):
                if re.search(r'(?P<olhos>\:|\;)(?P<nariz>\-|\~)?(?P<boca>\)|D)', i):
                    counter += 1

    return counter
