from pad_664.count_smileys import count_smileys
import unittest


class TestCountSmileys(unittest.TestCase):
    def test_count_smileys(self):

        self.assertEqual(count_smileys([':~]', ':-}', ':*', ':~', '=)', '=D', '=-D', ';-}']), 0)
        self.assertEqual(count_smileys([':)', ';(', ';}', ':-D']), 2)
        self.assertEqual(count_smileys([';D', ':-(', ':-)', ';~)']), 3)
        self.assertEqual(count_smileys([';]', ':[', ';*', ':$', ';-D']), 1)
        self.assertEqual(count_smileys([':}']), 0)
        self.assertEqual(count_smileys([':(']), 0)
        self.assertEqual(count_smileys([]), 0)
        self.assertEqual(count_smileys(()), 0)
        self.assertEqual(count_smileys({}), 0)
        self.assertEqual(count_smileys(54694), 0)